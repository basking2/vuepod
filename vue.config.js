
const https = require('https')

module.exports = {
  "devServer": {
    before: function(app, server, compiler) {
        app.use('/rss/:encodedUrl', (req, res, next) => {
            let url = Buffer.from(req.params['encodedUrl'], 'base64').toString('UTF-8')
            https.get(url, httpRes => {
                if (httpRes.statusCode == '200') {
 
                  res['Content-Type'] = httpRes.headers['Content-Type'] || 'application/rss+xml'
 
                  httpRes.on('data', chunk => res.write(chunk))
                    httpRes.on('end', () => {
                        res.end()
                    })
                    httpRes.on('error', () => {
                        res.end('Error')
                    })
                }
                else {
                    res.end('Error')
                }
            })
        })
    }
  },
  "transpileDependencies": [
    "vuetify"
  ]
}